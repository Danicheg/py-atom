from decimal import Decimal
from django import forms
from .models import Charge


class ChargeForm(forms.ModelForm):
    class Meta:
        model = Charge
        fields = ('date', 'value')

    def clean(self):
        data = super().clean()
        value_of_charge = data.get('value')
        date_of_charge = data.get('date')
        if value_of_charge is None or date_of_charge is None:
            return data
        if Decimal.compare(Decimal(0), value_of_charge) == Decimal('0'):
            self.add_error("value", "Danger! Charge can't be a zero!")
        if value_of_charge < 0 and date_of_charge > date_of_charge.today():
            self.add_error("date", "Danger! You can't set negative charge on future day!")
        return data

