from datetime import datetime
from django.core.urlresolvers import reverse
from django.db import models


class Charge(models.Model):
    date = models.DateField(default=datetime.today)
    value = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return "[" + str(self.date) + "] " + str(self.value)

    @staticmethod
    def get_absolute_url():
        return reverse("finances:finances")