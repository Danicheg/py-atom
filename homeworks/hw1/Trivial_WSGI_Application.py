from random import randint
from time import sleep, time
from datetime import datetime
from wsgiref.simple_server import make_server
from wsgiref.validate import validator


def log(message):
    print('[' + datetime.fromtimestamp(time()).strftime('%d-%m-%Y %H:%M:%S') + '] ' + message)


def events(max_delay, limit):
    while True:
        delay = randint(1, max_delay)
        if delay >= limit:
            sleep(limit)
            yield None
        else:
            sleep(delay)
            yield 'Event generated, awaiting %d s' % delay


EVENT_GENERATOR = events(4, 3)
HOST = '127.0.0.1'
PORT = 8080


class TrivialWSGIApplication:
    def __init__(self, environment, start_response):
        log('Get request')
        self.environment = environment
        self.start_response = start_response
        self.headers = [
            ('Content-type', 'text/plain; charset=utf-8')
        ]

    def __iter__(self):
        log('Wait for response')
        if self.environment.get('PATH_INFO', '/') == '/':
            event = next(EVENT_GENERATOR)
            log("Event: " + str(event))
            if event is None:
                self.no_content_response()
            else:
                yield from self.ok_response(event)
        else:
            yield from self.not_found_response()
        log('Done')

    def not_found_response(self):
        log('Create response')
        log('Send headers')
        error_message = "Wrong page. Go to http://localhost:" + str(PORT) + "/"
        self.start_response('404 Not Found', self.headers)
        yield (error_message).encode('utf-8')
        log('Headers in sent')

    # The server successfully processed the request and is not returning any content
    # https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html
    def no_content_response(self):
        log('Create response')
        self.headers = []
        self.start_response('204 No Content', self.headers)

    def ok_response(self, message):
        log('Create response')
        log('Send headers')
        self.start_response('200 OK', self.headers)
        log('Headers is sent')
        yield ('%s\n' % message).encode('utf-8')
        log('Body is sent')


if __name__ == '__main__':
    validate_app = validator(TrivialWSGIApplication)
    server = make_server(HOST, PORT, validate_app)
    server.serve_forever()
